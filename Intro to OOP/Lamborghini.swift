//
//  Lamborghini.swift
//  Intro to OOP
//
//  Created by R. Kukuh on 17/05/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

class Lamborghini {
    
    // MARK: Property
    
    var seatingCapacity: Int = 2
    var fuelType: String = "Petrol"
    var maxPower: String = "770bhp @8500rpm"
    
    // MARK: Method
    
    // methods goes here...
    
}
