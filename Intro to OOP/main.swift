//
//  main.swift
//  Intro to OOP
//
//  Created by R. Kukuh on 17/05/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

print("\t\tIntro to OOP")
print("---------------------------")

let myLambo = Lamborghini()

print("myLambo has \(myLambo.seatingCapacity) seats")

print("---------------------------")
print()
